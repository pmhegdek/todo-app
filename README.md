# README #


### What is this repository for? ###

A Todo app developed in MEAN stack
MongoDB,
 ExpressJs 4.2.0,
 Angular 4,
 NodeJs 4.2.0,
 Bootstrap 4

### How do I get set up? ###

* Clone the repository
* Go to directory /Todo-app/todo-client, install required modules using $ npm install, and build angular app using $ ng build
* Go to app base directory /Todo-app, install required modules using $ npm install
* Provide your mongodb url in /constants.keys.js
* After completing above setups start server using $ npm start
* visit http://localhost:3000/ in the browser.

