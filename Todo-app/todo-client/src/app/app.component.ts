import { Component, OnInit } from '@angular/core';
import { TodoService } from './todo.service'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TodoService]
})
export class AppComponent implements OnInit {

  private todoList: Object;
  private isNewClicked: Boolean;
  private isUpdateClicked: Boolean;
  private newCardDescription: String;
  private updateCardDescription: String;
  private updateCardId: String;
  private statuschanged;
  constructor(private todoService: TodoService) {
  }

  ngOnInit() {
    //initialize
    this.isNewClicked = false;
    this.todoService.init();
    this.getCard();
    this.statusChanged();
  }

  //receive cards
  getCard() {
    this.todoService.todoList.subscribe((data) => {
      this.todoList = data['data'];
    },
      (err) => {
        console.log(err);
      });
  }

  //status changed, refresh page
  statusChanged() {
    this.todoService.statusChanged.subscribe((data) => {
      this.isNewClicked = false;
      this.isUpdateClicked = false;
      console.log("status changed: reload window");
      window.location.reload();
    });
  }

  //show popup on clicking add new button
  newClicked() {
    this.isNewClicked = true;
  }

  //create new card
  createNewCard() {
    this.isNewClicked = false;
    this.todoService.createCard({
      "description": this.newCardDescription
    });
  }

  //on clicking update
  onupdateClick(id: String, descrptn: String) {
    this.updateCardId = id;
    this.isUpdateClicked = true;
    this.updateCardDescription = descrptn;
  }

  //update card
  updateCard() {
    this.todoService.updateCard({
      'id': this.updateCardId,
      'description': this.updateCardDescription
    });
  }

  //on clicking deleteCard
  deleteCard(cardId) {
    let obj = {
      "id": cardId
    };
    this.todoService.deleteCard(obj);
  }

  //onclicking cancel button
  cancel() {
    this.isNewClicked = false;
    this.isUpdateClicked = false;
    this.newCardDescription = '';
    this.updateCardDescription = '';
  }
}
