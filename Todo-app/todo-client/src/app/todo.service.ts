import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class TodoService {

  public todoList = new Subject();
  public myDetails = new Subject();
  public statusChanged = new Subject();
  constructor(private http: Http) { }


  init() {
    this.getCard();
  }
  //get my to do cards from server
  getCard(): void {
    this.http.get('/todo')
      .map(response => response.json()).subscribe(
      (response) => {
        this.todoList.next(response);
      }
      );
  }

  //create new card
  createCard(data) {
    this.http.post('/todo', data)
      .map(response => response.json()).subscribe(
      (response) => {
        this.statusChanged.next(response);
      }
      );
  }

  //update card
  updateCard(data) {
    this.http.put('/todo', data)
      .map(response => response.json()).subscribe(
      (response) => {
        this.statusChanged.next(response);
      }
      );
  }

  //delete card
  deleteCard(data) {
    this.http.delete('/todo', {
      params: {
        "id": data.id
      }
    })
      .map(response => response.json()).subscribe(
      (response) => {
        this.statusChanged.next(response);
      }
      );
  }
}
