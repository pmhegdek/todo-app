var express = require('express');
const path = require('path');
const random = require('random-number');

var router = express.Router();
const constants = require('../constants/constants');

let User = require('../model/user');

router.get('/', function (req, res) {
  let cookie = req.cookies.todoAppCookie;

  if (cookie == undefined) {

    let id = random({
      min: 10000000,
      max: 1000000000,
      integer: true
    });;
    console.log("New user: generted user_ID: " + id);
    res.cookie(constants.TODO_APP_COOKIE_NAME, id, {
      maxAge: 2 * 7 * 24 * 60 * 60 * 1000 //2 weeks
    });
  }

  console.log("user conncted: userId " + cookie);

  res.sendFile(path.join(__dirname, '../todo-client/dist/index.html'));
});

module.exports = router;
