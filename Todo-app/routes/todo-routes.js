var express = require('express');
var router = express.Router();

/* GET card listing. */
router.get('/', function (req, res) {
  //get cards
  let user = {
    username: req.cookies.todoAppCookie
  }
  let username = req.cookies.todoAppCookie;
  req.app.db.collection('todo').find(user).toArray(function (err, result) {
    if (err) {
      res.redirect('/');
    } else {
      if (result.length < 0) {
        res.json({
          "statuscode": 400,
          "message": "no-cards"
        });
      } else {
        res.json({
          "statuscode": 200,
          "data": result
        });
      }
    }
  });
});


//this api creates a new card in the databse
router.post('/', function (req, res) {
  //create new card
  let query = {
    username: req.cookies.todoAppCookie,
    description: req.body.description
  };

  if (query.username == null || query.username == undefined || query.username == "") {
    res.json({
      "statuscode": 400,
      "message": "invalid-user"
    });
    return;
  }
  req.app.db.collection('todo').insertOne(query, function (err, result) {
    if (err) {
      res.json({
        "statuscode": 400,
        "message": "db-error"
      });
    } else {
      res.json({
        "statuscode": 200,
        "message": "new-card-added"
      });
    }
  })
});

//this api updates an existing card
router.put('/', function (req, res) {
  //update card
  let update = {
    description: req.body.description
  };

  req.app.db.collection('todo').updateOne({ _id: new req.app.mongodb.ObjectID(req.body.id) }, { $set: update }, function (err, result) {
    if (err) {
      res.json({
        "statuscode": 203,
        "message": "db-error"
      });
    } else {
      res.json({
        "statuscode": 200,
        "message": "update-success"
      });
    }
  })
});

//this api deletes existing card
router.delete('/', function (req, res) {
  //delete card
  let id;

  if (req.body.id == undefined) {
    id = req.query.id;
  } else {
    id = req.body.id;
  }
  req.app.db.collection('todo').deleteOne({ _id: new req.app.mongodb.ObjectID(id) }, function (err, result) {
    if (err) {
      res.json({
        "statuscode": 203,
        "message": "db-error"
      });
    } else {
      res.json({
        "statuscode": 200,
        "message": "delete-success"
      });
    }
  })
});
module.exports = router;
