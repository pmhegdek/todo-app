const keys = require('../constants/keys');
//var app = require('../app');

var User = function () {
}

module.exports = User;
User.save = function (name, user_name, db, callback) {

  let userObj = {
    name: name,
    user_name: user_name
  };

  console.log(name);
  db.collection('todo-users').insertOne(userObj, function (err, result2) {
    if (err) {
      return callback(err)
    } else {
      //user saved successfully
      return (null, true);
    }
  });
};

User.isExists = function (name, user_name, db, callback) {
  let userObj = {
    name: name,
    user_name: user_name
  };
  db.collection('todo-users').find(userObj).toArray(function (err, result) {
    if (err) {
      return callback(err);
    } else {
      if (result.length > 0) {
        //user exists
        return callback(null, true);
      } else {
        //user not exists
        return callback(null, false);
      }
    }
  });
};