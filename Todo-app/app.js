const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');

//this stores the sesitive keys
const keys= require('./constants/keys');

const authRouter = require('./routes/auth-routes');
const todoRouter = require('./routes/todo-routes');
const constants= require('./constants/constants');
const mongodb= require('mongodb');
const MongoClient = mongodb.MongoClient;

console.log("connecting to mongodb.......")
//attach mongodb instance to app
MongoClient.connect(keys.MONGO_DB_URL, (err, database) => {
  if (err) {
    console.error("couldn't connect to mongDB");
    console.log(err);
    process.exit();
  } else {
    console.log('Mongodb connection successfull');
    app.db=database;
  }
});

let app = express();
app.mongodb=mongodb;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(session({
  secret:constants.SESSION_SECRET,
  saveUninitialized:true,
  resave:true,
  cookie:{
    maxAge:Infinity,
    domain:"localhost:3000"
  }
}));

app.use('/', authRouter);
app.use(express.static(path.join(__dirname, 'todo-client/dist')));


//app.use(express.static(path.join(__dirname, 'todo-client/dist')));
app.use('/todo', todoRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
